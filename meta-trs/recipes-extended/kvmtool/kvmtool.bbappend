
BB_URI := "${SRC_URI}"
BB_SRCREV := "${SRCREV}"

SRC_URI = "${@bb.utils.contains('MACHINE_FEATURES', 'arm64-cca', \
	   'git://gitlab.arm.com/linux-arm/kvmtool-cca;branch=cca/rmm-v1.0-eac5 \
	    file://external-crosscompiler.patch', \
	   '${BB_URI}', d)}"

SRCREV = "${@bb.utils.contains('MACHINE_FEATURES', 'arm64-cca', \
	   'e54eaed9536a2bb1bbf06d5f7210a8ba9707c728', \
	   '${BB_SRCREV}', d)}"
