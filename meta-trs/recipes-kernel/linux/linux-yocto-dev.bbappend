
FILESEXTRAPATHS:prepend := "${THISDIR}/files/:"

SRC_URI:append = "file://defconfig \
		  file://dm-crypt.cfg \
		  file://tpm-ftpm-tee.cfg \
		  file://0001-kvm-arm64-Include-kvm_emulate.h-in-kvm-arm_psci.h.patch \
		  file://0002-arm64-RME-Handle-Granule-Protection-Faults-GPFs.patch \
		  file://0003-arm64-RME-Add-SMC-definitions-for-calling-the-RMM.patch \
		  file://0004-arm64-RME-Add-wrappers-for-RMI-calls.patch \
		  file://0005-arm64-RME-Check-for-RME-support-at-KVM-init.patch \
		  file://0006-arm64-RME-Define-the-user-ABI.patch \
		  file://0007-arm64-RME-ioctls-to-create-and-configure-realms.patch \
		  file://0008-kvm-arm64-Expose-debug-HW-register-numbers-for-Realm.patch \
		  file://0009-arm64-kvm-Allow-passing-machine-type-in-KVM-creation.patch \
		  file://0010-arm64-RME-Keep-a-spare-page-delegated-to-the-RMM.patch \
		  file://0011-arm64-RME-RTT-handling.patch \
		  file://0012-arm64-RME-Allocate-free-RECs-to-match-vCPUs.patch \
		  file://0013-arm64-RME-Support-for-the-VGIC-in-realms.patch \
		  file://0014-KVM-arm64-Support-timers-in-realm-RECs.patch \
		  file://0015-arm64-RME-Allow-VMM-to-set-RIPAS.patch \
		  file://0016-arm64-RME-Handle-realm-enter-exit.patch \
		  file://0017-KVM-arm64-Handle-realm-MMIO-emulation.patch \
	 	  file://0018-arm64-RME-Allow-populating-initial-contents.patch \
		  file://0019-arm64-RME-Runtime-faulting-of-memory.patch \
		  file://0020-KVM-arm64-Handle-realm-VCPU-load.patch \
		  file://0021-KVM-arm64-Validate-register-access-for-a-Realm-VM.patch \
		  file://0022-KVM-arm64-Handle-Realm-PSCI-requests.patch \
		  file://0023-KVM-arm64-WARN-on-injected-undef-exceptions.patch \
		  file://0024-arm64-Don-t-expose-stolen-time-for-realm-guests.patch \
		  file://0025-arm64-rme-allow-userspace-to-inject-aborts.patch \
		  file://0026-arm64-rme-support-RSI_HOST_CALL.patch \
		  file://0027-arm64-rme-Allow-checking-SVE-on-VM-instance.patch \
		  file://0028-arm64-RME-Always-use-4k-pages-for-realms.patch \
		  file://0029-arm64-rme-Prevent-Device-mappings-for-Realms.patch \
		  file://0030-arm_pmu-Provide-a-mechanism-for-disabling-the-physic.patch \
		  file://0031-arm64-rme-Enable-PMU-support-with-a-realm-guest.patch \
		  file://0032-kvm-rme-Hide-KVM_CAP_READONLY_MEM-for-realm-guests.patch \
		  file://0033-KVM-arm64-Allow-activating-realms.patch \
		  file://0034-arm64-remove-redundant-extern.patch \
		  file://0035-arm64-rsi-Add-RSI-definitions.patch \
		  file://0036-arm64-Detect-if-in-a-realm-and-set-RIPAS-RAM.patch \
		  file://0037-arm64-realm-Query-IPA-size-from-the-RMM.patch \
		  file://0038-arm64-Mark-all-I-O-as-non-secure-shared.patch \
		  file://0039-fixmap-Allow-architecture-overriding-set_fixmap_io.patch \
		  file://0040-arm64-Override-set_fixmap_io.patch \
		  file://0041-arm64-Make-the-PHYS_MASK_SHIFT-dynamic.patch \
		  file://0042-arm64-Enforce-bounce-buffers-for-realm-DMA.patch \
		  file://0043-arm64-Enable-memory-encrypt-for-Realms.patch \
		  file://0044-arm64-Force-device-mappings-to-be-non-secure-shared.patch \
		  file://0045-efi-arm64-Map-Device-with-Prot-Shared.patch \
		  file://0046-arm64-realm-Support-nonsecure-ITS-emulation-shared.patch \
		  file://0047-arm64-rme-Add-test-module-for-Realm-granule-access.patch" 

KBRANCH = "v6.7/standard/qemuarm64"
LINUX_VERSION = "6.7"
SRCREV_machine = "5c416ebbb12d0227bfd4a4b3c8a3d02c84080682"

do_compile:append() {
    oe_runmake -C ${B} dtbs
}

do_install:append() {
    oe_runmake -C ${B} DEPMOD=echo INSTALL_DTBS_PATH=${D}/boot/dtb dtbs_install
}

do_install:append() {
    if [ ! -d ${D}/boot/dtb ]; then
        # force the creation of dtb directory on boot to have
        install -d ${D}/boot/dtb
        echo "Empty content on case there is no devicetree" > ${D}/boot/dtb/.emtpy
    fi

    #rename device tree
    for dtb in ${DTB_RENAMING}
    do
        dtb_orignal=$(echo $dtb | cut -d':' -f 1 )
        dtb_renamed=$(echo $dtb | cut -d':' -f 2 )

        if [ -f ${D}/boot/$dtb_orignal ]; then
            cd ${D}/boot/
            ln -s $dtb_orignal $dtb_renamed
           cd -
        fi
        if [ -f ${D}/boot/dtb/$dtb_orignal ]; then
            cd ${D}/boot/dtb/
            ln -s $dtb_orignal $dtb_renamed
            cd -
        fi
    done
}

do_deploy:append() {
        mkdir -p ${DEPLOYDIR}/dtb
    if [ -d ${WORKDIR}/package/boot/dtb ];then
        cp -rf ${WORKDIR}/package/boot/dtb ${DEPLOYDIR}/
    fi
}
