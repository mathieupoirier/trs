# reduce build dependencies
PACKAGECONFIG:remove = "plugin_modem_manager"

# tpm, uefi support
PACKAGECONFIG:append = " plugin_tpm plugin_uefi_pk plugin_uefi_capsule "
