# reduce emulated targets to
QEMU_TARGETS = "i386 aarch64"

FILESEXTRAPATHS:prepend := "${THISDIR}/files/:"

CCA_PATCHES = "file://0001-target-arm-helper-Propagate-MDCR_EL2.HPMN-into-PMCR_.patch \
               file://0002-kvm-Merge-kvm_check_extension-and-kvm_vm_check_exten.patch \
               file://0003-NOMERGE-Add-KVM-Arm-RME-definitions-to-Linux-headers.patch \
               file://0004-target-arm-Add-confidential-guest-support.patch \
               file://0005-target-arm-kvm-rme-Initialize-realm.patch \
               file://0006-hw-arm-virt-Add-support-for-Arm-RME.patch \
               file://0007-hw-arm-virt-Disable-DTB-randomness-for-confidential-.patch \
               file://0008-hw-arm-virt-Reserve-one-bit-of-guest-physical-addres.patch \
               file://0009-target-arm-kvm-Split-kvm_arch_get-put_registers.patch \
               file://0010-target-arm-kvm-rme-Initialize-vCPU.patch \
               file://0011-hw-core-loader-Add-ROM-loader-notifier.patch \
               file://0012-target-arm-kvm-rme-Register-a-ROM-loader-notifier.patch \
               file://0013-hw-arm-boot-Register-Linux-BSS-section-for-confident.patch \
               file://0014-target-arm-kvm-rme-Add-Realm-Personalization-Value-p.patch \
               file://0015-target-arm-kvm-rme-Add-measurement-algorithm-propert.patch \
               file://0016-target-arm-kvm-rme-Add-Realm-SVE-vector-length.patch \
               file://0017-target-arm-kvm-rme-Add-breakpoints-and-watchpoints-p.patch \
               file://0018-target-arm-kvm-rme-Add-PMU-num-counters-parameters.patch \
               file://0019-target-arm-kvm-Disable-Realm-reboot.patch \
               file://0020-target-arm-kvm-rme-Disable-readonly-mappings.patch \
               file://0021-target-arm-cpu-Inform-about-reading-confidential-CPU.patch \
               file://0022-target-arm-gdbstub-Disable-accessing-registers-of-a-.patch \
               file://0023-hw-arm-virt-Move-virt_flash_create-to-machvirt_init.patch \
               file://0024-hw-arm-virt-Use-RAM-instead-of-flash-for-confidentia.patch"

SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "arm64-cca", "${CCA_PATCHES}", "", d)}"
